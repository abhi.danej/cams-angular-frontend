import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-infomodal',
  templateUrl: './infomodal.component.html'
})
export class InfoModalComponent implements OnInit {

  @Input() message: string;
  @Input() isSuccess: boolean;

  constructor() { }

  ngOnInit() {
    console.log("info modal onInit")
  }

}
