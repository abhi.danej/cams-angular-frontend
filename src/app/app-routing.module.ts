import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BannerComponent } from './banner/banner.component';
import { UsersComponent } from './admin/users/users.component'
import { CreateUserComponent } from './admin/users/create-user/create-user.component'
import { SearchuserComponent } from './admin/users/searchuser/searchuser.component'
import { ModifyUserComponent } from './admin/users/modify-user/modify-user.component'
import { GroupsComponent } from './admin/groups/groups.component';
import { SearchGroupComponent } from './admin/groups/search-group/search-group.component';
import { ModifyGroupComponent } from './admin/groups/modify-group/modify-group.component';


const routes: Routes = [
  { path: '', component: BannerComponent },
  {
    path: 'admin/groups', component: GroupsComponent, children: [
      { path: '', component: SearchGroupComponent },
      { path: 'group/:id', component: ModifyGroupComponent }
    ]
  },
  {
    path: 'admin/users', component: UsersComponent, children: [
      { path: '', component: SearchuserComponent },
      { path: 'user/:id', component: ModifyUserComponent },
      { path: 'new', component: CreateUserComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponents = [
  BannerComponent,
  UsersComponent,
  SearchuserComponent,
  ModifyUserComponent,
  CreateUserComponent
];
