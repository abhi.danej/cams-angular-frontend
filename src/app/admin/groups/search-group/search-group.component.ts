import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../admin.service';
import { UserGroup } from '../../admin-model';

@Component({
  selector: 'app-search-group',
  templateUrl: './search-group.component.html'
})
export class SearchGroupComponent implements OnInit {

  groupList: UserGroup[];

  constructor(private service: AdminService) { }

  ngOnInit() {
  }

  searchGroups(value: String) {
    console.log("Search group request with value: " + value);
      console.log("Search user request for: " + value)
  
      this.service.searchGroups(value).subscribe(
        data => {
          this.groupList = data;
          console.log(data);
        },
        error => {
          console.log("Error: " + JSON.stringify(error));
          alert(JSON.stringify(error));
        });
  }
}
