import { Component, OnInit, Input } from '@angular/core';
import { UserGroup } from '../../admin-model';

@Component({
  selector: 'app-group-list',
  templateUrl: './group-list.component.html'
})
export class GroupListComponent implements OnInit {

  @Input() groupList: UserGroup[];
  constructor() { }

  ngOnInit() {
  }

}
