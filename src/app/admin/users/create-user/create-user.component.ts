import { Component, OnInit } from '@angular/core';
import { User, Role } from '../../admin-model'
import { AdminService } from '../../admin.service';

// use jQuery
declare var $: any;

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styles: []
})
export class CreateUserComponent implements OnInit {
  
  user: User = new User();
  userRoles: Role[] = new Array();
  modalMessage: String = "";
  isSuccess: boolean;

  roles = [
    { id: 1, role: 'ROLE_ADMIN' },
    { id: 2, role: 'ROLE_USER' },
  ]
  constructor(private adminService: AdminService) { }

  ngOnInit() {
  }
  
  createUser(userForm) {
    
    // also put in reset
    this.user.id = 0;
    // this.user.active = 0;
    this.userRoles = [];
    this.user.password = this.user.email;

    console.log(userForm);
    if (this.user.active) {
      this.user.active = 1;
    } else {
      this.user.active = 0;
    }
    if(userForm.role_admin) {
      this.roles.forEach(role => {
        if(role.role === 'ROLE_ADMIN') {
          this.userRoles.push({id: role.id, role: role.role});
        }
      });
    }
    if(userForm.role_user) {
      this.roles.forEach(role => {
        if(role.role === 'ROLE_USER') {
          this.userRoles.push({id: role.id, role: role.role});
        }
      });
    }
    this.user.roles = this.userRoles;
    console.log(this.user);
    // USER PREPARATION OVER //
    

    this.adminService.createUser(this.user).subscribe(
      (data: any) => {
        console.log(data);
        this.showModal(true, data.msg);
      },
      error => {
        console.log("Error: " + JSON.stringify(error));
        alert(JSON.stringify(error));
      });
      
  }

  showModal(isSuccess, message): void {
    this.modalMessage = message;
    this.isSuccess = isSuccess;
    $("#info-modal").modal('show')
  }
}
