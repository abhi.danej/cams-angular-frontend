import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from 'src/app/admin/admin.service';
import { User, Role } from 'src/app/admin/admin-model';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
})
export class UserListComponent implements OnInit {

  user: User;
  roleList: Role[];
  @Input() userList;
  constructor(private router: Router, private adminService: AdminService) { }

  ngOnInit() {
  }

  navigateToModifyUser(id: number) {

    console.log("Navigating to userid: " + id);

    this.adminService.getRoleList()
      .subscribe((data: Role[]) => {
        this.roleList = data;
        console.log("Recieved roleList: " + JSON.stringify(data));
      })

    this.adminService.getUserById(id)
      .subscribe((data: User) => {
        this.user = data;
        console.log("Recieved user: " + JSON.stringify(this.user) + "/////// " + JSON.stringify(this.roleList));
        this.router.navigate(
          ['admin','users','user', id],
          { state: {data: this.user, roleList: this.roleList} }
        );
      })

    // this.router.navigateByUrl('user', { state: { hello: 'world' } });
  }



}
