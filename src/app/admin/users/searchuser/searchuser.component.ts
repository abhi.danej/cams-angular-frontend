import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../admin.service'
import { User } from '../../admin-model'

@Component({
  selector: 'app-searchuser',
  templateUrl: './searchuser.component.html'
})
export class SearchuserComponent implements OnInit {

  public userList: User[];
  
  constructor(private service: AdminService) { }

  ngOnInit() {

      // this.service.getUserList().subscribe(
      //   data => {
      //     this.userList = data;
      //     console.log(data);
      //   },
      //   error => {
      //     console.log("Error: " + JSON.stringify(error));
      //     alert(JSON.stringify(error));
      //   });
  }

  searchUser(value: String) {
    console.log("Search user request for: " + value)

    this.service.searchUsers(value).subscribe(
      data => {
        const userList: User[] = data;
        this.userList = userList;
        console.log(data);
      },
      error => {
        console.log("Error: " + JSON.stringify(error));
        alert(JSON.stringify(error));
      });
  }

}

