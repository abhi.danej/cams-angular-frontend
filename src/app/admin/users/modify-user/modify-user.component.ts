import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminService } from '../../admin.service';
import { User, Role } from '../../admin-model'
import { map } from 'rxjs/operators'
import { Observable } from 'rxjs'

// use jQuery
declare var $: any;

@Component({
  selector: 'app-modify-user',
  templateUrl: './modify-user.component.html',
  styles: []
})
export class ModifyUserComponent implements OnInit {

  public user: User;
  roleList: Role[];
  modalMessage: String = "";
  isSuccess: boolean;
  // state: Observable<Object>;

  constructor(private route: ActivatedRoute, private adminService: AdminService) {
  }

  ngOnInit() {

    // this.state = this.route.paramMap
    // .pipe(map(() => history.state.data));

    // console.log("State is: ")
    // console.log(this.state)

    this.route.paramMap.subscribe(() => {
      this.user = window.history.state.data;
      this.roleList = window.history.state.roleList;
      console.log("Role data is: " + JSON.stringify(this.roleList));
      console.log("User data is: " + JSON.stringify(this.user));
    })

    // .pipe(map(() => history.state.data));

    // console.log("State is: ")
    // console.log(this.state)

    // this.route.paramMap.subscribe(params => {

    //   const id = +params.get('id');
    //   console.log("calling getUserById with service " + id);

    //   this.adminService.getUserById(id)
    //     .subscribe((data: User) => {
    //       this.user = data;
    //       console.log(this.user);
    //     })
    // })
  }

  diagnostic(obj) {
    return JSON.stringify(obj);
  }

  saveUser(userForm) {
    console.log(userForm.value)
    this.adminService.modifyUser(this.user);
    var modifiedUser: User = {
      id: this.user.id,
      email: this.user.email,
      name: this.user.name,
      lastName: this.user.lastName,
      active: this.user.active,
      roles: this.user.roles,
      password: this.user.password
    }

    this.adminService.modifyUser(modifiedUser).subscribe(
      (data: any) => {
        console.log(data);
        this.showModal(true, data.msg);
      },
      error => {
        console.log("Error: " + JSON.stringify(error));
        alert(JSON.stringify(error));
      });
  }

  showModal(isSuccess, message): void {
    this.modalMessage = message;
    this.isSuccess = isSuccess;
    $("#info-modal").modal('show')
  }

  onClickUserActiveCheckbox(value: boolean) {
    if (value) {
      this.user.active = 1
    } else {
      this.user.active = 0;
    }
  }

  isUserRole(id: number) {
    console.log("checking for role.id: " + id);
    var value: boolean = false;
    // const val = this.user.roles.findIndex(e => {
    //   e.id === id;
    // })
    // console.log("Found index: " + val);
    // return false;
    console.log("length of user.roles: " + this.user.roles.length);
    console.log(JSON.stringify(this.user.roles))
    if(this.user.roles.length > 0) {

      this.user.roles.forEach(userRole => {
        console.log("testing against user role: " + userRole.id)
        if(+userRole.id === id) {
          console.log("matched")
          value = true;
        }
      });
    }
    return value;
  }

  onClickUserRoleCheckbox(index: number, event: any) {
    console.log("Event Object: " + JSON.stringify(event));
    console.log("Current value of: " + index +" : " + event.target.checked + ".")
    if(event.target.checked) {
      console.log("selected role: " + JSON.stringify(this.roleList[index]))
      this.user.roles.push(this.roleList[index])
    } else {
      var ar = this.user.roles.filter(elm => {
        elm.id !== this.roleList[index].id;
      })
      this.user.roles = ar;
    }
  }

}
