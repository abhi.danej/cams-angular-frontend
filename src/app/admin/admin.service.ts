import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';
import { User, Role, UserGroup } from './admin-model'
import { Observable, throwError } from 'rxjs'
import { map, catchError } from 'rxjs/operators';


@Injectable()
export class AdminService {

    baseUrl = 'http://localhost:8080'
    constructor(private http: HttpClient) { }

    getUserList() {
        //return this.http.get<User[]>(this.baseUrl + '/userList');
        return this.http.get<User[]>(this.baseUrl + '/userList')
            .pipe(
                map(response => {
                    return response;
                }),
                catchError(this.handleError)
            );
    }

    searchUsers(searchValue: any) {
        var queryParam: String;
        if (searchValue.match(/^[0-9]+$/)) {
            queryParam = "?id=" + searchValue;
        } else {
            queryParam = "?name=" + searchValue;
        }

        return this.http.get<User[]>(this.baseUrl + '/searchUsers' + queryParam)
            .pipe(
                map(response => {
                    return response;
                }),
                catchError(this.handleError)
            );
    }

    getUserById(id: number) {
        return this.http.get<User>(this.baseUrl + '/user/' + id);
        // add error handling.
    }

    getRoleList() {
        return this.http.get<Role[]>(this.baseUrl + '/roleList')
            .pipe(
                map(response => {
                    return response;
                }),
                catchError(this.handleError)
            );
    }

    createUser(user: User) {
        return this.http.post<any>(this.baseUrl + '/user', user)
            .pipe(
                map(response => {
                    return response;
                }),
                catchError(this.handleError)
            );
    }

    modifyUser(user: User) {
        return this.http.post<any>(this.baseUrl + '/user', user)
            .pipe(
                map(response => {
                    return response;
                }),
                catchError(this.handleError)
            );
    }

    searchGroups(searchValue: any) {
        var queryParam: String;
        if (searchValue.match(/^[0-9]+$/)) {
            queryParam = "?id=" + searchValue;
        } else {
            queryParam = "?name=" + searchValue;
        }

        return this.http.get<UserGroup[]>(this.baseUrl + '/searchGroups' + queryParam)
            .pipe(
                map(response => {
                    return response;
                }),
                catchError(this.handleError)
            );
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}, ${error.message}`);
        }
        // return an observable with a user-facing error message
        return throwError(
            'Server error: ' + error.message);
    };
}