export class UserGroup {
  id: number;
  name: string;
  description: string;
  members: User[];
  active: boolean;
}

export class User {
  id: number;
  email: string;
  name: string;
  password: string;
  lastName: string;
  active: number;
  roles: Role[];
}

export class Role {
  id: number;
  role: string;
}

