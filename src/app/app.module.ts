import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome'
import { faCoffee,faUser,faCheckCircle } from '@fortawesome/free-solid-svg-icons'

import { NavbarComponent } from './navbar/navbar.component';
import { UsersComponent } from './admin/users/users.component';
import { SearchuserComponent } from './admin/users/searchuser/searchuser.component';
import { UserListComponent } from './admin/users/searchuser/user-list/user-list.component';
import { ModifyUserComponent } from './admin/users/modify-user/modify-user.component';
import { AdminService } from './admin/admin.service';
import { CreateUserComponent } from './admin/users/create-user/create-user.component';
import { InfoModalComponent } from './notifications/infomodal.component';
import { GroupsComponent } from './admin/groups/groups.component';
import { GroupListComponent } from './admin/groups/group-list/group-list.component';
import { SearchGroupComponent } from './admin/groups/search-group/search-group.component';
import { ModifyGroupComponent } from './admin/groups/modify-group/modify-group.component'


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    routingComponents,
    UsersComponent,
    SearchuserComponent,
    UserListComponent,
    ModifyUserComponent,
    CreateUserComponent,
    InfoModalComponent,
    GroupsComponent,
    GroupListComponent,
    SearchGroupComponent,
    ModifyGroupComponent
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [AdminService],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faCoffee,faUser,faCheckCircle);
  }
 }
